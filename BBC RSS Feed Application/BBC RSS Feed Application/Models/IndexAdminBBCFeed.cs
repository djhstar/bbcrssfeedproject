﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using Newtonsoft.Json;
using BBC_RSS_Feed_Application.Models;

namespace BBC_RSS_Feed_Application.Models
{
    public class IndexAdminBBCFeed
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public List<FeedBBC> BBCFeedList { get; set; }
    
        public IndexAdminBBCFeed() {
            XDocument Xdoc = new XDocument();
            Xdoc = ReadFeedBBC.GetXML();
            this.BBCFeedList = new List<FeedBBC>();
            this.Title = Xdoc.Descendants("title").First().Value.ToString();
            this.Link = Xdoc.Descendants("link").First().Value.ToString();
            this.Description = Xdoc.Descendants("description").First().Value.ToString();

            
        }

    }

}