﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;

namespace BBC_RSS_Feed_Application.Models
{
    public class FeedBBC
    {
        public string Title {get; set;}
        public string Description { get; set;}
        public string Link { get; set;}
        public string PubDate { get; set;}
    }

    public class ReadFeedBBC
    {
        public static List<FeedBBC> GetFeed()
        {
            var client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            var xmlData = client.DownloadString("http://feeds.bbci.co.uk/news/uk/rss.xml?edition=uk");

            XDocument xml = XDocument.Parse(xmlData);

            

            var BBCUpdates = (from story in xml.Descendants("item")
                              select new FeedBBC
                              {
                                  Title = ((string)story.Element("title")),
                                  Description = ((string)story.Element("description")),
                                  Link = ((string)story.Element("link")),
                                  PubDate = ((string)story.Element("pubDate"))
                              }).ToList();

            return BBCUpdates;


        }

        public static XDocument GetXML()
        {
            var client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            var xmlData = client.DownloadString("http://feeds.bbci.co.uk/news/uk/rss.xml?edition=uk");

            XDocument xml = XDocument.Parse(xmlData);

            return xml;


        }
    }
}