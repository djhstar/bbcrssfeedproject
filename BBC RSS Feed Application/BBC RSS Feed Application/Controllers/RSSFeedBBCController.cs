﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using Newtonsoft.Json;
using BBC_RSS_Feed_Application.Models;

namespace BBC_RSS_Feed_Application.Controllers
{
    public class RSSFeedBBCController : Controller
    {
        // GET: RSSFeedBBC
        public ActionResult Index()
        {

            IndexAdminBBCFeed model = new IndexAdminBBCFeed();

            model.BBCFeedList = ReadFeedBBC.GetFeed();
            return View(model);
        
        }

        public ActionResult Save()
        {
            try
            {

                IndexAdminBBCFeed Feed = new IndexAdminBBCFeed();

                Feed.BBCFeedList = ReadFeedBBC.GetFeed();

                Feed = CheckForDuplicates(Feed);

                string jsonText = JsonConvert.SerializeObject(Feed, Newtonsoft.Json.Formatting.Indented);

                string FileName = DateTime.Now.ToString("yyyy-MM-dd-H") + ".json";

                System.IO.File.WriteAllText(@"C:\Users\David\BBCRSSFeedData\" + FileName, jsonText);

                return Json(new { savedOK = true }, JsonRequestBehavior.AllowGet);

            }

            catch
            {
                return Json(new { savedOK = false }, JsonRequestBehavior.AllowGet);
            }
            

        }

        public IndexAdminBBCFeed CheckForDuplicates(IndexAdminBBCFeed Feed) {

            IndexAdminBBCFeed OldFeed = new IndexAdminBBCFeed();

            OldFeed = LoadDayNewsItems();
            NewsItemsComparer NewsComparer = new NewsItemsComparer();

            var FeedList = Feed.BBCFeedList.Except(OldFeed.BBCFeedList, (NewsComparer));

            if (FeedList.FirstOrDefault() == null||FeedList == null|| FeedList.Any(s => string.IsNullOrEmpty(s.Title)))
            {
                FeedList = new List<FeedBBC>();
            }
            Feed.BBCFeedList = FeedList.ToList();
         
            return Feed;

        }

        public IndexAdminBBCFeed LoadDayNewsItems()
        {
            XmlDocument xml = new XmlDocument();

            IndexAdminBBCFeed OldFeed = new IndexAdminBBCFeed();

            foreach (string file in Directory.EnumerateFiles(@"C:\Users\David\BBCRSSFeedData\", "*.json")) 
            {
                string contents = System.IO.File.ReadAllText(file);

                xml = JsonConvert.DeserializeXmlNode(contents, "root");

                XDocument OldDocs = XDocument.Parse(xml.OuterXml);

                List<FeedBBC> BBCUpdates = (from story in OldDocs.Descendants("BBCFeedList")
                                  select new FeedBBC
                                  {
                                      Title = ((string)story.Element("Title")),
                                      Description = ((string)story.Element("Description")),
                                      Link = ((string)story.Element("Link")),
                                      PubDate = ((string)story.Element("PubDate"))
                                  }).ToList();

                OldFeed.BBCFeedList.AddRange(BBCUpdates);

            }


            return OldFeed;
        }
    }

    public class NewsItemsComparer : IEqualityComparer<FeedBBC>
    {
        public bool Equals(FeedBBC x, FeedBBC y)
        {
            return
                x.Title == y.Title &&
                x.Description == y.Description &&
                x.Link == y.Link &&
                x.PubDate == y.PubDate;

            }

        public int GetHashCode(FeedBBC obj)
        {
            if (obj.Title != null)
            {
                return obj.Title.GetHashCode();
            }
            else
            {
                return obj.GetHashCode();
            }
        }
  
    }
}